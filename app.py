import pyautogui
import time
from typing import Tuple, List, Dict, Any
from mss import mss
import cv2
import imutils
import numpy
from MTM import matchTemplates, drawBoxesOnRGB
from enum import Enum
import pandas as pd
import itertools
import asyncio

GAME_WIDTH = 767
GAME_HEIGHT = 434

GAME_TOP = 521
GAME_LEFT = 888
# Will be calculated from screen width if not set

DRAW_AREA_OFFSET = 100
# Top-left offset from where we want to start to draw the shapes

DRAW_SPEED = 0.001
DRAW_SCALE = 0.4
# Cursor move time

DEBUG = False

pyautogui.MINIMUM_DURATION = 0.0005
pyautogui.MINIMUM_SLEEP = 0.00025

screen_width, _ = pyautogui.size()
if not GAME_LEFT:
    GAME_LEFT = int((screen_width - GAME_WIDTH) / 2)


class Move:
    def __init__(self, vectors: List[Tuple[int, int]], start: Tuple[int, int] = (0, 0)):
        self.start = start
        self.vectors = vectors

    def execute(self):
        start_x, start_y = self.start
        pyautogui.moveTo(GAME_LEFT+DRAW_AREA_OFFSET+(start_x*DRAW_SCALE),
                         GAME_TOP+DRAW_AREA_OFFSET+(start_y*DRAW_SCALE))
        pyautogui.mouseDown()
        for x, y in self.vectors:
            pyautogui.moveRel(x*DRAW_SCALE, y*DRAW_SCALE, duration=DRAW_SPEED, tween=pyautogui.easeInOutQuad)
        pyautogui.mouseUp()


class Symbols(Enum):
    DASH = "dash"
    LINE = "line"
    ARROW_DOWN = "arrow_down"
    ARROW_UP = "arrow_up"
    SPIRAL = "spiral"
    LIGHTNING = "lightning"
    HEART = "heart"
    BUBBLE = "bubble"


move_vectors = {
    Symbols.DASH: Move([(50, 0)]),
    Symbols.LINE: Move([(0, 50)]),
    Symbols.ARROW_DOWN: Move([(20, 50), (20, -50)]),
    Symbols.ARROW_UP: Move([(20, -50), (20, 50)], (0, 50)),
    Symbols.SPIRAL: Move([(0, 50), (50, 0), (0, -50), (-25, 0), (0, 25), (10, 0)]),
    Symbols.LIGHTNING: Move([(-20, 30), (20, 0), (-20, 30)], (20, 0)),
}

symbol_width_bgr_map = {
    Symbols.HEART: (26, [190, 156, 255]),
    Symbols.ARROW_UP: (30, [66, 249, 0]),
    Symbols.DASH: (24, [107, 107, 255]),
    Symbols.SPIRAL: (28, [221, 136, 184]),
    Symbols.ARROW_DOWN: (28, [40, 252, 255]),
    Symbols.BUBBLE: (24, [255, 253, 0]),
    Symbols.LIGHTNING: (20, [1, 187, 252]),
    Symbols.LINE: (10, [255, 154, 82]),
}


# Half-stolen from https://stackoverflow.com/a/31402351
def bbox(img):
    alpha = img[:, :, 3]
    rows = numpy.any(alpha, axis=1)
    cols = numpy.any(alpha, axis=0)
    ymin, ymax = numpy.where(rows)[0][[0, -1]]
    xmin, xmax = numpy.where(cols)[0][[0, -1]]
    return img[ymin:ymax + 1, xmin:xmax + 1]


def image_color_mask(img, bgr):
    t = 15  # color threshold
    b, g, r = bgr
    mask = (img[:, :, 0] < b+t) & (img[:, :, 1] < g+t) & (img[:, :, 2] < r+t) \
           & (img[:, :, 0] > b-t) & (img[:, :, 1] > g-t) & (img[:, :, 2] > r-t)
    return mask


def imshow_mask(name, mask):
    img = numpy.full((mask.shape[0], mask.shape[1], 3), [0, 0, 0], dtype="uint8")
    img[mask] = [255, 255, 255]
    cv2.imshow(name, img)


async def main():
    # time.sleep(3)
    # for name, vector in move_vectors.items():
    #     print("Drawing", name)
    #     vector.execute()
    #     time.sleep(1)

    # Load symbols
    symbols_image = cv2.imread("symbols.png", cv2.IMREAD_UNCHANGED)

    # Split symbols into their own images
    # Get list of offsets
    offset = 0
    split_map: List[int] = []
    for width, _ in list(symbol_width_bgr_map.values())[:-1]:
        offset += width
        split_map.append(offset)
    # Split array at offsets
    symbol_images = numpy.split(symbols_image, split_map, 1)
    symbol_map = {symbol: symbol_images[i] for i, symbol in enumerate(symbol_width_bgr_map.keys())}

    # if DEBUG:
    #     for name, img in symbol_map.items():
    #         cv2.imshow(name.name, img)

    # Scale symbols into multiple versions so we can find different scales within the screenshot
    scaled_symbols_map: Dict[Symbols, List[Tuple[float, Any]]] = {}
    for symbol, image in symbol_map.items():
        # 5 versions from 100% to 200%
        for scale in numpy.linspace(0.8, 1.5, 8):
            # Resize by width
            resized = imutils.resize(image, width=int(image.shape[1] * scale))
            # Apply alpha bbox, apply color based mask
            resized = image_color_mask(bbox(resized)[:, :, :3], symbol_width_bgr_map[symbol][1])
            if symbol not in scaled_symbols_map:
                scaled_symbols_map[symbol] = []
            scaled_symbols_map[symbol].append((scale, resized,))

    labelled_images = {}
    for symbol, resized_images in scaled_symbols_map.items():
        labelled_images[symbol] = [(f"{symbol.name}:{scale}", image) for scale, image in resized_images]
    # labelled_images = {symbol: [(symbol.name, image)] for symbol, image in symbol_map.items()}

    screenshot_bbox = GAME_LEFT, GAME_TOP, GAME_LEFT+GAME_WIDTH, GAME_TOP+GAME_HEIGHT
    screenshot_center = GAME_WIDTH/2, GAME_HEIGHT/2

    lowest_detected_scale = None
    highest_detected_scale = None

    with mss() as sct:

        while True:
            # Take the screenshot, convert to numpy array, remove leftmost 32px (we don't want detections there),
            # remove alpha channel
            screenshot_image = numpy.array(sct.grab(screenshot_bbox))[:, 32:, :3]

            if DEBUG:
                cv2.imshow("screenshot", screenshot_image)
                cv2.waitKey(0)
                cv2.destroyAllWindows()

            start = time.time()

            def process(symbol, bgr) -> pd.DataFrame:
                bw = image_color_mask(screenshot_image, bgr)

                # if DEBUG:
                #     imshow_mask(f"color mask for {symbol.name}", bw)
                #     cv2.waitKey(0)

                return matchTemplates(labelled_images[symbol], bw,
                                      score_threshold=1, method=cv2.TM_SQDIFF_NORMED)

            df_futs = []
            loop = asyncio.get_running_loop()
            for symbol, (_, bgr) in symbol_width_bgr_map.items():
                df_futs.append(loop.run_in_executor(None, process, symbol, bgr))
            df = pd.concat(await asyncio.gather(*df_futs))

            # if DEBUG:
            #     cv2.destroyAllWindows()

            print(f"Image processing took {time.time() - start:0.02f}s")

            if DEBUG:
                print("Results:", len(df))
            if not len(df):
                await asyncio.sleep(0.01)
                if DEBUG:
                    break
                else:
                    continue

            # Initial sorting by y offset for grouping
            df.sort_values(by="BBox", key=lambda df: df.str[1], inplace=True)
            # Grouping by y offset (kinda stolen from https://stackoverflow.com/a/51541718)
            df.loc[(df.BBox.shift().str[1] < df.BBox.str[1] - 10), 'group'] = 1
            # use cumsum, ffill and fillna to complete the column group and have a different number for each one
            df['group'] = df['group'].cumsum().ffill().fillna(0)

            # Sort back to x offsets
            df.sort_values(by="BBox", inplace=True)
            # print(df)

            groups = [group for _, group in df.groupby(by="group")]
            print("Groups:", len(groups))

            # If there are multiple groups, only get their first element each
            if len(groups) > 1:
                first_rows = True
                rows = [group.iloc[0] for group in groups]
            # Otherwise, take up to the first 4 elements (arbitrary number, helps for boss fights etc)
            else:
                first_rows = False
                rows = [row for _, row in df[:4].iterrows()]

            print(rows)

            if DEBUG:
                overlay = drawBoxesOnRGB(screenshot_image, df, showLabel=True)
                cv2.imshow(f"results", overlay)
                print(df)
                cv2.waitKey(0)

            detection_count = {}
            moves = []

            for row in rows:
                symbol_name, scale = row["TemplateName"].split(":")
                symbol = Symbols[symbol_name]
                scale = float(scale)
                if not highest_detected_scale or scale > highest_detected_scale:
                    highest_detected_scale = scale
                if not lowest_detected_scale or scale < lowest_detected_scale:
                    lowest_detected_scale = scale
                # print(highest_detected_scale, lowest_detected_scale)
                if symbol not in move_vectors:
                    continue
                # Calculate combined offset to center
                # center_offset = abs(screenshot_center[0] - row["BBox"][0]) + abs(screenshot_center[1] - row["BBox"][1])
                if symbol not in detection_count:
                    detection_count[symbol] = 1
                else:
                    detection_count[symbol] += 1
                # If only first of each group, only append one of each (one draw removes all of the same symbol)
                if not first_rows or not next(filter(lambda m: m[0] == symbol, moves), None):
                    moves.append((symbol, move_vectors[symbol], _,))

            def key(move):
                symbol, _, distance = move
                # If move is a spiral, give it highest priority
                if symbol == Symbols.SPIRAL:
                    return -100
                else:
                    return -detection_count[symbol]
                # Otherwise, go by combined offset to screenshot center
                # else:
                #     return distance

            if first_rows:
                moves.sort(key=key)

            print("Planned moves in order:", [symbol for symbol, _, _ in moves])
            if not DEBUG:
                # if len(moves):
                for symbol, vector, _ in moves:
                #     symbol, vector, _ = moves[0]
                    vector.execute()

            if DEBUG:
                break
        # cv2.destroyAllWindows()


if __name__ == "__main__":
    asyncio.run(main())
