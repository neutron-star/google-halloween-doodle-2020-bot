Halloween 2020 Google Doodle Bot
================================

To use: Install Python 3.7+ and everything from `requirements.txt`.  
The Doodle can be found in the archive: https://www.google.com/doodles/halloween-2020

Current status
--------------

**C** "there is hope" tier. Dies in level 4:
https://youtu.be/upPIsSUbiJY